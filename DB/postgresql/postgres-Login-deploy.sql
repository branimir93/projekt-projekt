-- Database: "Login"

-- DROP DATABASE "Login";

CREATE DATABASE "Login"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;



-- Table: usertable

-- DROP TABLE usertable CASCADE;


CREATE TABLE userTable (
	uid SERIAL PRIMARY KEY,
	username varchar(50) not null UNIQUE,
	password varchar(50) not null,
	fname varchar(50) not null,
	lname varchar(50) not null,
	email varchar(50) not null,
	regdate TIMESTAMP DEFAULT NOW()
);

--Indexes on: userTable

CREATE INDEX username_index ON userTable(username);

--Sample data:
INSERT INTO USERTABLE (USERNAME,PASSWORD,FNAME,LNAME,EMAIL) 
	VALUES('korisnik1','korisnik1','KorisnikIme','KorisnikPrezime','korisnik@mail.com');

INSERT INTO USERTABLE (USERNAME,PASSWORD,FNAME,LNAME,EMAIL) 
	VALUES('ivo','ivo','IVAN','IVI�','ivoivic@mail.com');

select * from usertable;



-- Table: dbsettings

-- DROP TABLE dbsettings CASCADE;



CREATE TABLE DBSETTINGS (
	DBID SERIAL PRIMARY KEY,
	DBALIAS varchar(50) not null,

	--for database connection
	DBTYPE varchar(50) not null,
	username varchar(50) not null, 
	password varchar(50) not null,
	databaseName varchar(50) not null,
	server varchar(50) not null,
	port integer not null,
	userID integer references userTable(uid) ON  DELETE CASCADE,
	
	--constraints
	CONSTRAINT valid_port CHECK(port > 0),
	CONSTRAINT unique_db  UNIQUE(DBALIAS,userID)
);


--Indexes on: userTable

CREATE INDEX database_user_index ON dbsettings(DBALIAS,userID);

--Sample data:
insert into dbsettings(DBALIAS,dbtype,username,password,databaseName,server,port,userID) 
	values('MyDatabase1','mysql','user1','pass','sampleDatabase','localhost',555,1);

insert into dbsettings(DBALIAS,dbtype,username,password,databaseName,server,port,userID) 
	values('MyDatabase2','mysql','user1','pass','sampleDatabase2','localhost',555,1);


select * from DBSETTINGS,userTable 
	where dbsettings.userID = userTable.uid;
	
	
-- UserTracking:

--DROP TABLE userTracking CASCADE;

CREATE TABLE userTracking (
	utid SERIAL PRIMARY KEY,
	visitTime TIMESTAMP DEFAULT NOW(),
	userID integer references userTable(uid) ON  DELETE CASCADE
);

insert into userTracking(userID) values(1);
insert into userTracking(userID) values(2);
insert into userTracking(userID) values(2);

select * from userTracking;