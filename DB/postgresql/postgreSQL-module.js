var pg = require('pg');


var connectionString = "";
var client = null;
var database = null;
var dbid;

module.exports = {

    /**
    *	Function connects to database. This must be first method called.
    *	Function takes object for connecting to database. 
    *	Object must have following fields: 
    *		user,password,host,port,databaseName.
    *
    * Example: 
    *	var connection = new Object();
    *		connection.user = "postgres";
    *		connection.password = "passwd";
    *		connection.host = "localhost";
    *		connection.port = "5432";
    *		connection.databaseName = "DatabaseName";
    *
    *   loginDB.connect(connection);
    */
    connect: function(con,errorFunction) {
        //constring: postgres://user:password@host:port/database
        this.database = database;
        connectionString = "postgres://" + con.user + ":" 
                + con.password + "@" + con.host + 
                ":" + con.port +"/" + con.databaseName;
    
            client = new pg.Client(connectionString);
            client.connect(function(err) {
                if(err) {
                    errorFunction(err);
                }else {
                    errorFunction(null);
                }
            });
        dbid = con.dbid;
    },

    /**
    *	Function disconnects from database.
    */
        disconnect: function() {
            if(client != null) {
                client.end();
            }
    },

    /**
    *	Function returns connection string of currently connected database.
    *
    */
    getConnectionString: function() {
    	return connectionString;
    },


    /**
    *	Function executes custom query on currenty connected database.
    *	Takes two arguments: custom query and processing function.
    *	Processing function takes one argument: object with field result.
    *	Result contains array of results with key and values. Key denotes attribute name and value denotes attribute value.
    *   If there is error processing query, hasError is set to true and message is written in errorMessage field.
    *   NOTE: all keys are same for every element in array.
    *
    */
    executeQuery: function(queryString,processQuery) {
        client.query(queryString,function(err,result) { 
            if(err) {
                var totalResult = {};
                totalResult.hasError = true;
                totalResult.error = err.message;
                processQuery(totalResult);					
            } else {
                var totalResult = {};
                totalResult.result = result.rows;
                processQuery(totalResult);
            }
        });
    },

    /**
    *	Function returns all databases in database.
    *	Processing function processQuery takes one parameter, 
    *		result with field: databases, which is an array of databases.
    *
    */
    getDatabases: function(processQuery) {

        var totalResult = {};
        var resultArray = [];
        var prepStatement = "select * from pg_database where datistemplate = false;";
        client.query(prepStatement,function(err,result) {
    
            for(var i = 0; i < result.rows.length; i++) {
                resultArray.push(result.rows[i].datname);
            }
            totalResult.databases = resultArray;
            processQuery(totalResult);
			
    	});
    },
    
    /**
    *	Function returns all tables from current database.
    *	Processing function processQuery takes one parameter, 
    *		result with field: tables, which is an array of tables.
    *
    */
    getTables: function(processQuery) {

        var resultArray = [];
        
        var prepStatement = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' and table_type='BASE TABLE';";
	
        client.query(prepStatement,function(err,result) {
                
                for(var i = 0; i < result.rows.length; i++) {
                    var jsonObject =  {
                        attr: { type: "table", dbid: dbid, name: result.rows[i].table_name },
                        data: result.rows[i].table_name,
                    };
                    resultArray[i] = jsonObject;
                }
                
                processQuery(resultArray);
        });
    },



    /**
    *	Function returns all procedures from current database.
    *	Processing function processQuery takes one parameter, result.
    *
    */
    getProcedures: function(processQuery) {

        var resultArray = [];
        
        var prepStatement = "SELECT proname FROM pg_catalog.pg_namespace n JOIN pg_catalog.pg_proc p ON pronamespace = n.oid WHERE nspname = 'public';";
	
        client.query(prepStatement,function(err,result) {
                
                for(var i = 0; i < result.rows.length; i++) {
                    var jsonObject =  {
                        attr: { type: "procedure", dbid: dbid, name: result.rows[i].proname  },
                        data: result.rows[i].proname,
                    };
                    resultArray[i] = jsonObject;
                }
                processQuery(resultArray);
        });
    },
    
    getProcedureData: function(procedureName, processQuery) {

        var totalResult = {};
        
        var prepStatement = "SELECT proname as procname, prosrc as procsrc FROM pg_catalog.pg_namespace n JOIN pg_catalog.pg_proc p ON pronamespace = n.oid WHERE nspname = 'public' AND proname='" + procedureName + "';"  ;
	
        client.query(prepStatement,function(err,result) {
                
                if(err) {
                    console.log(err);
                    totalResult.error = err;
                    processQuery(totalResult);
                }else if(result.rows.length === 0) {
                    processQuery(null);
                }
                else {
                    totalResult.result = result.rows[0];
                    processQuery(totalResult);
                }
                
                
        });
    },
    
   /**
    *	Function returns all views from current database.
    *	Processing function processQuery takes one parameter, result.
    *
    */
    getViews: function(processQuery) {

        var resultArray = [];
        
        var prepStatement = "select * from INFORMATION_SCHEMA.views WHERE table_schema = ANY (current_schemas(false));";
	
        client.query(prepStatement,function(err,result) {
                
                for(var i = 0; i < result.rows.length; i++) {
                    var jsonObject =  {
                        attr: { type: "view", dbid: dbid, name: result.rows[i].table_name  },
                        data: result.rows[i].table_name,
                    };
                    resultArray[i] = jsonObject;
                }
                
                processQuery(resultArray);
        });
    },
    
    getViewData: function(viewName,processQuery) {

        var prepStatement = "select TABLE_NAME as viewname, VIEW_DEFINITION as viewsrc from INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = '"+viewName+"';";
	
        client.query(prepStatement,function(err,result) {
                
                var totalResult = {};
                
                if(err) {
                    
                    totalResult.error = err;
                    processQuery(totalResult);
                    
                } else if(result.rows.length === 0) {
                    processQuery(null);
                } else {
                    
                    totalResult.result = result.rows[0];
                    processQuery(totalResult);
                }
                
                
        });
    },
    
    /**
     * Function returns first 10 records in table.
     * Function takes two arguments, table name and process function.
     * 
     */
    getRecordsFromTable: function(tableName,number,processQuery) {
        var prepStatement = "SELECT * FROM " + tableName +   " LIMIT " + number+";"; //TODO: prepstatement
        
        client.query(prepStatement,function(err,result) {
            if(err) {
                
                console.log(prepStatement);
                var totalResult = {};
                totalResult.hasError = true;
                totalResult.error = err;
                console.log(err);
                processQuery(totalResult);					
            } else {
                 var totalResult = {};
                totalResult.result = result.rows;
                processQuery(totalResult);
            }
        });
    },
    
    getTableInfo: function(table_name,processQuery){
        var prepStatement = "SELECT * from INFORMATION_SCHEMA.COLUMNS where table_name = '" + table_name + "';";
        client.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            } else {
               
                var resultArray = [];
                
                for(var i = 0; i < result.rows.length; i++) {
                
                    var j = {};
                    j.column_name = result.rows[i].column_name;
                    j.ordinal_position = result.rows[i].ordinal_position;
                    j.column_default = result.rows[i].column_default;
                    j.is_nullable = result.rows[i].is_nullable;
                    j.data_type = result.rows[i].data_type;
                    j.column_default = result.rows[i].column_default;
       
                    resultArray[i] = j;
                }
                processQuery(resultArray);
            }
        
        });
    },
    
    getPrimaryKeys: function(database_name,processQuery){
        
        var prepStatement = "select * from information_schema.table_constraints where constraint_type='PRIMARY KEY' and table_catalog='"+ database_name +"';";
        client.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            } 
            else if (result.rows.length === 0 ) {
				processQuery(null);
            }
            else {
                processQuery(result.rows);
            }
        
        });
    },
    
    getPrimaryKeysFromTable: function(database_name,table_name,processQuery){
        
        var prepStatement = "select * from information_schema.table_constraints where constraint_type='PRIMARY KEY' and table_name = '" 
            +table_name + "' and table_catalog='"+ database_name +"';";
        client.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            } 
            else if (result.rows.length === 0 ) {
				processQuery(null);
            }
            else {
                processQuery(result.rows);
            }
        
        });
    },
    
    getForeignKeys: function(database_name,processQuery){
        
        var prepStatement = "select * from information_schema.table_constraints where constraint_type='FOREIGN KEY' and table_catalog='"+ database_name +"';";
        client.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            }
            else if (result.rows.length === 0 ) {
				processQuery(null);
            }
            else {
                processQuery(result.rows);
            }
        
        });
    },
    
    getForeignKeysFromTable: function(database_name,table_name,processQuery){
        
        var prepStatement = "select * from information_schema.table_constraints where constraint_type='FOREIGN KEY' and table_catalog='"
            + database_name +"' and table_name = '"+ table_name +"';";
        
        client.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            } else if (result.rows.length === 0 ) {
				processQuery(null);
            }
            else {
                processQuery(result.rows);
            }
        
        });
    },
    
    getTableData: function(database_name,table_name,processQuery) {
        var totalResult = {};
        this.getPrimaryKeysFromTable(database_name,table_name,function(result) {
                totalResult.PK = result;
        });   
        this.getForeignKeysFromTable(database_name,table_name,function(result){
               totalResult.FK = result; 
        });   
		this.getRecordsFromTable(table_name,2000000,function(result) {
                totalResult.Content = result;
		});
        this.getTableInfo(table_name,function(result) {
                totalResult.Collumns = result;
                processQuery(totalResult);
		});
	}
    
};