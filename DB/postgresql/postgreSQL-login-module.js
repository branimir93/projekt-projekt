var pg = require('pg');
var connectionString;
var client;
var database;

/**
 * Hardcoded variables for json.
 */


                    
var jsonDatabaseState = "closed";
                    
/**
*	Function connects to database. This must be first method called.
*	Function takes object for connecting to database. 
*	Object must have following fields: 
*		user,password,host,port,databaseName.
*
* Example: 
*	var connection = new Object();
*		connection.user = "postgres";
*		connection.password = "passwd";
*		connection.host = "localhost";
*		connection.port = "5432";
*		connection.databaseName = "Login";
*
*   loginDB.connect(connection);
*/
exports.connect = function(con,errorFunction) {
	//constring: postgres://user:password@host:port/database
	this.database = database;
	connectionString = "postgres://" + con.user + ":" 
						+ con.password + "@" + con.host + 
						":" + con.port +"/" + con.databaseName;

		client = new pg.Client(connectionString);
		client.connect(function(err) {
			if(err) {
				errorFunction(err);
			}
		});
}


/**
*	Function disconnects from database.
*/
exports.disconnect = function() {
	if(client != null) {
		client.end();
	}
}

/*

*************** FUNCTIONS FOR USERS********
	
*/
/**
*	Function returns user by username.
*	function processUser takes one argument: false if there is no user with that username,
*	or if there is result, user object. User object contains fields: uid (user ID), username,password,
*	fname(first name), lname(last name), email and regdate (registration date).
*/
exports.getUserByUsername = function(username,processUser) {
	
	var prepStatement = 'SELECT * FROM userTable WHERE username = $1';
	
	client.query(prepStatement,[username],function(err,result) { 

		if((err != null) || (result.rowCount == 0)) {
			processUser(false);
		} else {
			processUser(result.rows[0]);
		}
	});
	
}

/**
*	Function adds user to database.
*	Function takes two arguments: user, and error processing function that takes one parameter, error.
*	User must have fields: username, password, fname, lname, email.
*
*/
exports.addUser = function(user,processError) {
	
	var prepStatement = 'INSERT INTO USERTABLE (USERNAME,PASSWORD,FNAME,LNAME,EMAIL) \
						VALUES($1,$2,$3,$4,$5);';

	client.query(prepStatement,[user.username,user.password,user.fname,user.lname,user.email],function(err,result) { 
		processError(err);
	});
	
}

/**
*	Function remoces user from database by his username.
*	Function takes two arguments: username, and error processing function that takes one parameter, error.
*
*/
exports.removeUserByUsername = function(username,processError) {
	
	var prepStatement = 'DELETE FROM userTable WHERE username = $1;';
	
	client.query(prepStatement,[username],function(err,result) { 
		processError(err);
	});
	
}

/*

*************** FUNCTIONS FOR DATABASES ********
	
*/
exports.getDatabasesOfUserByUsername = function(username,processDatabases) {
	
	var prepStatement = 'SELECT * FROM dbSettings where userid = \
			(Select UID from userTable where username = $1)';
	
	client.query(prepStatement,[username],function(err,result) { 
		
		if((err != null) || (result.rowCount == 0)) {
			processDatabases(false);
		} else {
			var array = new Array();
            for(var i = 0; i < result.rows.length; i++) {
                //result.rows[i];
                
                var databaseChildren = [ 
                        {
                            attr: { type: "table", dbid: result.rows[i].dbid},
                            data: "Tablice",
                            state: "closed"
                        },
                        {
                            attr: { type: "procedure", dbid: result.rows[i].dbid},
                            data: "Procedure",
                            state: "closed",
                        },
                        {
                            attr: { type: "views", dbid: result.rows[i].dbid},
                            data: "Pogledi",
                            state: "closed",
                        },
                    ];
                    
                var jsonObject = {
                    attr : { 
                            type: "db", 
                            dbid: result.rows[i].dbid,
                            username: result.rows[i].username, 
                            password: result.rows[i].password, 
                            databasename: result.rows[i].databasename,
                            server: result.rows[i].server,
                            port: result.rows[i].port,
                            dbtype: result.rows[i].dbtype
                        },
                    data : result.rows[i].dbalias,
                    children : databaseChildren,
                    state : jsonDatabaseState
                }
                array[i] = jsonObject;
            }
            processDatabases(array);
		}
	});
	
}


exports.addUserDatabase = function(username,database,processError) {
	
	this.getUserByUsername(username,function(user) {
	
		var prepStatement = 'insert into dbsettings(dbalias,dbtype,username,password,databasename,server,port,userID) \
			values($1,$2,$3,$4,$5,$6,$7,$8);';

		client.query(prepStatement,[database.dbalias,database.dbtype,database.username,database.password,database.databasename,database.server,database.port,user.uid],function(err,result) { 
			processError(err);
		});
	});
}


exports.removeDatabaseByUsername = function(dbalias,username,processError) {
	
	var prepStatement = 'DELETE FROM DBSettings \
	WHERE	userid = (SELECT UID from userTable where username = $1) \
		AND DBAlias = $2;';
	
	client.query(prepStatement,[username,dbalias],function(err,result) { 
		processError(err);
	});
	
}


/*

*************** FUNCTIONS FOR VISITS********
	
*/
exports.getVisitsOfUserByUsername = function(username,processVisits) {
	
	var prepStatement = 'SELECT * FROM userTracking where userid = \
			(Select UID from userTable where username = $1) ORDER BY VISITTIME';
	
	client.query(prepStatement,[username],function(err,result) { 
		
		if((err != null) || (result.rowCount == 0)) {
			processVisits(false);
		} else {
			processVisits(result.rows);
		}
	});
	
}

