var mysql = require('mysql');
var dbid;
var database;

module.exports = {

    
    /*
     * funkcija za spajanje na bazu podataka
     */
     
    connect: function(userInfo, errorFunction){
            
        this.connection = mysql.createConnection(
            {
                user:       userInfo.user,
                password:   userInfo.password,
                host:       userInfo.host,
                port:       userInfo.port,
                database:   userInfo.databaseName
            }
        );
        dbid = userInfo.dbid;
        database = userInfo.databaseName;
        
        this.connection.connect(function(err) {
            if(err) {
                errorFunction(err);
            }else {
                errorFunction(null);
            }
        });

    },
    
    
    /*
     * fukcija koja vraca popis svih tablica i podataka u obliku za prikaz u jsTree-u
     */
     
    getTables: function(processQuery){
        
        this.connection.query(
            "SELECT table_name FROM information_schema.tables WHERE table_schema = DATABASE() and  TABLE_TYPE LIKE 'BASE_TABLE' ORDER BY table_name ASC", 
            function(err, results){
				//console.log(err);
				var array = [];
                for(var i = 0; i < results.length; i++) {
                    var jsonObject = {
                        attr : { type: "table", dbid: dbid, name: results[i].table_name },
                        data : results[i].table_name,
                    };
                    array[i] = jsonObject;
                }
				processQuery(array);
				
        });
    },
    
    /*
     * Diskonektaj bazu
     */
    disconnect: function(){
        if(this.connection === null) {
            
        }else {
            this.connection.end();
        }
        
    },
    
    
    /*
     * funkcija za izvrsavanje SQL upita
     */
    executeQuery: function (SQLstring,result){
        var queryStr = new Array();
        var len = SQLstring.length;
        queryStr = SQLstring.substring(0, len-2);
        this.connection.query(SQLstring,
            function(err, results){
                if(err) {
                    var r = {};
                    r.error = err.message;
                    result(r);
                }else {
                    var totalResult = {};
                    totalResult.result = results;
                    result(totalResult);
  
                }
	
            }
        );
    },
    
    
    /* 
     *funkcija koja vraca popis svih procedura iz baze
     */
    getProcedures: function(processQuery){

        this.connection.query(
            "SELECT * FROM mysql.proc", 
            function(err, results){
				if(err) {
				    console.log(err);
				    processQuery([{data: err.code}]);
				} else {
    				var array = [];
                    for(var i = 0; i < results.length; i++) {
                        
                        var jsonObject = {
                            attr : { type: "procedure", dbid: dbid, name: results[i].name },
                            data : results[i].name,
                        };
                        array[i] = jsonObject;
                    }
                    if(results.length == 0){
                        processQuery([{data: "Nema procedura"}]);
                    }else {
    				    processQuery(array);
                    }
				}
        });
    },
    getProcedureData: function(procedureName, processQuery) {

        var totalResult = {};
        
        var prepStatement = "SELECT ROUTINE_NAME AS procname, ROUTINE_DEFINITION AS procsrc FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = DATABASE() AND ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_NAME = '" + procedureName +"';";
	
        this.connection.query(prepStatement,function(err,result) {
                
                if(err) {
                    console.log(err);
                    totalResult.error = err;
                    processQuery(totalResult);
                }else if(result.length === 0) {
                    processQuery(null);
                }
                else {
                    totalResult.result = result[0];
                    processQuery(totalResult);
                }
                
                
        });
    },
    getViewData: function(viewName,processQuery) {

        var prepStatement = "SELECT TABLE_NAME as viewname, VIEW_DEFINITION as viewsrc FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = '" + viewName +"';";
	
        this.connection.query(prepStatement,function(err,result) {
                
                var totalResult = {};
                
                if(err) {
                    
                    totalResult.error = err;
                    processQuery(totalResult);
                    
                } else if(result.length === 0) {
                    processQuery(null);
                } else {
                    
                    totalResult.result = result[0];
                    processQuery(totalResult);
                }
                
                
        });
    },
    
    getPrimaryKeys: function(database_name,processQuery){
        
        var prepStatement = "select * from information_schema.table_constraints where constraint_type='PRIMARY KEY' and table_catalog='"+ database_name +"';";
        this.connection.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            } else {
                processQuery(result);
            }
        
        });
    },
    
    getPrimaryKeysFromTable: function(database_name,table_name,processQuery){
        
        var prepStatement = "select * from information_schema.table_constraints where constraint_type='PRIMARY KEY' and table_name = '" 
            +table_name + "' and table_schema='"+ database_name +"';";
        this.connection.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            } else if (result.length === 0){
                processQuery(null);
            } else {
                processQuery(result);
            }
        });
    },
    
    getForeignKeysFromTable: function(database_name,table_name,processQuery){
        
        var prepStatement = "select * from information_schema.table_constraints where constraint_type='FOREIGN KEY' and table_name='"
            + table_name +"'and constraint_schema='"+ database_name +"';";
        this.connection.query(prepStatement,function(err,result){
            
            if(err) {
                var totalResult = {};
                totalResult.error = err;
                processQuery(totalResult);
            } else if (result.length === 0){
                processQuery(null);
            } else {
                processQuery(result);
            }
        
        });
    },
    
    getRecordsFromTable: function(tableName,number, processQuery) {
        var prepStatement = "SELECT * FROM " + tableName +   ";"; 
        
        this.connection.query(prepStatement,function(err,result) {
            
            if(err) {
                var totalResult = new Object();
                totalResult.hasError = true;
                totalResult.error = err;
                //console.log(err);
                processQuery(totalResult);					
            } else {
                var totalResult = {};
                totalResult.result = {};
                totalResult.result = result;
               //console.log(totalResult);
                processQuery(totalResult);
            }
        });
    },
    
    
    getViews: function(processQuery) {

        var resultArray = new Array();
        
        var prepStatement = "SELECT * FROM information_schema.TABLES WHERE TABLE_TYPE LIKE 'VIEW' and TABLE_SCHEMA IN (SELECT DATABASE());";
	
        this.connection.query(prepStatement,function(err,results) {
                
                for(var i = 0; i < results.length; i++) {
                    var jsonObject =  {
                        attr: { type: "view", dbid: dbid, name: results[i].TABLE_NAME, source:results[i].TABLE_SCHEMA  },
                        data: results[i].TABLE_NAME,
                    };
                    resultArray[i] = jsonObject;
                }
                
                processQuery(resultArray);
        });
    },
    /*
    *funkcija koja vraca info o tablicama:
    */
    
    getTableInfo: function(tableName,processQuery){
            this.connection.query(
            "SELECT column_name, ordinal_position, column_default, is_nullable, data_type FROM INFORMATION_SCHEMA.Columns WHERE table_schema = DATABASE() AND TABLE_NAME ='"+tableName +"' ORDER BY table_name DESC;", 
            function(err, results){
                if(err) {
                    console.log(err);
                    processQuery(err);
                }else {
                    processQuery(results);
                }
                
            }
        );
    }
};