var mssql = require('mssql');
var databaseid;

/*config = {
    user: 'sa',
    password: '0000',
    host: 'localhost',
	port: '1433',
    databaseName: 'pubs'
} */

module.exports = {
    
    connect: function(config, errorFunction){
        
        var parameters = {
			user: config.user,
			password: config.password,
			server: config.host,
			port: config.port,
			database: config.databaseName
		};
		databaseid = config.dbid;
		
		this.connection = new mssql.Connection(parameters, function(err) {
	
			if (err) {
				errorFunction(err);
			} else {
				errorFunction(null);
			}
		});	
	},
    
    
    disconnect: function(){
        this.connection.close();
    },
    
    
    getTables: function(returnResult){
		
		var request = this.connection.request();
        request.query( "SELECT * FROM information_schema.tables WHERE TABLE_TYPE = 'BASE TABLE' ORDER BY table_name ASC;", function(err, recordset){
				var array = [];
                for(var i = 0; i < recordset.length; i++) {               
                    var jsonObject = {
                        attr : { type: "table", dbid: databaseid, name: recordset[i].TABLE_NAME },
                        data : recordset[i].TABLE_NAME,
                    };
                    array[i] = jsonObject;
                }
				returnResult(array); 
            }
        );
    },


    executeQuery: function (upit, returnResult){
        
		var queryResult = {};
		var request = new mssql.Request(this.connection);
        request.query( upit, function(err, recordset){
                if (err){
					queryResult.hasError = true;
					queryResult.error = err.message;
					returnResult(queryResult);
				} else {
					queryResult.result = recordset;
					returnResult(queryResult);
				}
            }
        );
    },
    
    
    getRecordsFromTable: function(tableName,number,returnResult) {
        
        var queryResult = {};
        var request = new mssql.Request(this.connection);
        var prepStatement = "SELECT TOP " + number +" * FROM [" +tableName +"];";
        request.query(prepStatement, function(err, recordset){
            if(err){			
				queryResult.error = err.message;
				returnResult(queryResult);
			} else if (recordset.length === 0 ) {
				queryResult.result = null;
				returnResult(queryResult);
			} else {
				queryResult.result = recordset;
				returnResult(queryResult);
			}
        });
    },  
    
    
    getProcedures: function(returnResult){
		var request = new mssql.Request(this.connection);
        request.query( "SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE';", function(err, recordset){
				var array = [];
                for(var i = 0; i < recordset.length; i++) {
                    var jsonObject = {
                        attr : { type: "procedure", dbid: databaseid , name:recordset[i].ROUTINE_NAME },
                        data : recordset[i].ROUTINE_NAME,
                    };
                    array[i] = jsonObject;
                }
                returnResult(array);
            }
        );
    },
    
    
    getProcedureData: function(procedureName ,returnResult){
		var queryResult = {};
		var request = new mssql.Request(this.connection);
        request.query( "SELECT ROUTINE_NAME AS procname, ROUTINE_DEFINITION AS procsrc FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_NAME ='" + procedureName + "';", function(err, recordset){
			if(err){
                queryResult.error = err;
                returnResult(queryResult);
            } else if (recordset.length === 0 ) {
                returnResult(null);
            } else {
				queryResult.result = recordset[0];
				returnResult(queryResult);
            }
        });
    },
    
    
    getViews: function(returnResult){
		var request = new mssql.Request(this.connection);
        request.query( "SELECT * FROM INFORMATION_SCHEMA.VIEWS;", function(err, recordset){
				var array = [];
                for(var i = 0; i < recordset.length; i++) {
                    var jsonObject = {
                        attr : { type: "view", dbid: databaseid , name:recordset[i].TABLE_NAME },
                        data : recordset[i].TABLE_NAME,
                    };
                    array[i] = jsonObject;
                }
                returnResult(array);
            }
        );
    },
    
    
    getViewData: function(viewName ,returnResult){
		var queryResult = {};
		var request = new mssql.Request(this.connection);
        request.query( "SELECT TABLE_NAME as viewname, VIEW_DEFINITION as viewsrc FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME='" + viewName + "';", function(err, recordset){
			if(err){
                queryResult.error = err.message;
                returnResult(queryResult);
            } else if (recordset.length === 0 ) {
                returnResult(null);
            } else {
				queryResult.result = recordset[0];
				returnResult(queryResult);
            }
        });
    },
    
    
    getTableInfo: function(tableName, returnResult){
		
		var request = new mssql.Request(this.connection);
        request.query( "SELECT * FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME ='" + tableName + "';", function(err, recordset){
			if(err){			
				var queryResult = {};
				queryResult.error = err.message;
				returnResult(queryResult);
			} else {
				var array = [];
                for(var i = 0; i < recordset.length; i++) {               
                    var j = {};
                    j.column_name = recordset[i].COLUMN_NAME;
                    j.ordinal_position = recordset[i].ORDINAL_POSITION;
                    j.is_nullable = recordset[i].IS_NULLABLE;
                    j.data_type = recordset[i].DATA_TYPE;
                    j.column_default = recordset[i].COLUMN_DEFAULT;
                    array[i] = j;
                }
				returnResult(array);
			}
        });
    },
    
    
    getViewContents: function(viewName, returnResult){
		
		var request = new mssql.Request(this.connection);
        request.query( "SELECT * FROM [" + viewName + "];", function(err, recordset){				
				returnResult(recordset); 
            }
        );
    },
    
	
	getPrimaryKeysFromTable: function(databaseName, tableName, returnResult){
		
		var request = new mssql.Request(this.connection);
		
		console.dir(databaseName);
		var keyQuery =  "select tc.CONSTRAINT_TYPE, kcu.COLUMN_NAME, kcu.ORDINAL_POSITION\
								from INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc\
								join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as kcu\
									on kcu.CONSTRAINT_SCHEMA = tc.CONSTRAINT_SCHEMA\
									and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME\
									and kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA\
									and kcu.TABLE_NAME = tc.TABLE_NAME\
							where tc.CONSTRAINT_TYPE in ('PRIMARY KEY')\
									and kcu.TABLE_NAME = '" + tableName + "'\
									and kcu.TABLE_CATALOG = '" + databaseName + "'\
							order by kcu.ORDINAL_POSITION;";
        request.query(keyQuery , function(err, recordset){
			
			if(err){			
				var queryResult = {};
				queryResult.error = err.message;
				returnResult(queryResult);
			} else if (recordset.length === 0 ) {
				returnResult(null);
			} else {
				returnResult(recordset);
			}
        });
    },

	
	getPrimaryKeys: function(databaseName, returnResult){
		
		var request = new mssql.Request(this.connection);
		var keyQuery = "select kcu.TABLE_NAME, tc.CONSTRAINT_TYPE, kcu.COLUMN_NAME, kcu.ORDINAL_POSITION\
								from INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc\
								join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as kcu\
									on kcu.CONSTRAINT_SCHEMA = tc.CONSTRAINT_SCHEMA\
									and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME\
									and kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA\
									and kcu.TABLE_NAME = tc.TABLE_NAME\
							where tc.CONSTRAINT_TYPE in ('PRIMARY KEY')\
									and kcu.CONSTRAINT_CATALOG = '" + databaseName + "'\
							order by kcu.ORDINAL_POSITION;";
        request.query(keyQuery , function(err, recordset){
			
			if(err){			
				var queryResult = {};
				queryResult.error = err.message;
				returnResult(queryResult);
			} else if (recordset.length === 0 ) {
				returnResult(null);
			} else {
				returnResult(recordset);
			}
        });
    },	
	
	
	getForeignKeysFromTable: function(databaseName, tableName, returnResult){
		
		var request = new mssql.Request(this.connection);
		var keyQuery = "select tc.CONSTRAINT_TYPE, kcu.COLUMN_NAME, kcu.ORDINAL_POSITION\
								from INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc\
								join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as kcu\
									on kcu.CONSTRAINT_SCHEMA = tc.CONSTRAINT_SCHEMA\
									and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME\
									and kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA\
									and kcu.TABLE_NAME = tc.TABLE_NAME\
							where tc.CONSTRAINT_TYPE in ('FOREIGN KEY')\
									and kcu.TABLE_NAME = '" + tableName + "'\
									and kcu.TABLE_CATALOG = '" + databaseName + "'\
							order by kcu.ORDINAL_POSITION;";
        request.query(keyQuery , function(err, recordset){
			
			if(err){			
				var queryResult = {};
				queryResult.error = err.message;
				returnResult(queryResult);
			} else if (recordset.length === 0 ) {
				returnResult(null);
			} else {
				returnResult(recordset);
			}
        });
    },

	
	getForeignKeys: function(databaseName, returnResult){
		
		var request = new mssql.Request(this.connection);
		var keyQuery = "select kcu.TABLE_NAME, tc.CONSTRAINT_TYPE, kcu.COLUMN_NAME, kcu.ORDINAL_POSITION\
								from INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc\
								join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as kcu\
									on kcu.CONSTRAINT_SCHEMA = tc.CONSTRAINT_SCHEMA\
									and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME\
									and kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA\
									and kcu.TABLE_NAME = tc.TABLE_NAME\
							where tc.CONSTRAINT_TYPE in ('FOREIGN KEY')\
									and kcu.TABLE_CATALOG = '" + databaseName + "'\
							order by kcu.ORDINAL_POSITION;";
        request.query(keyQuery , function(err, recordset){
			
			if(err){			
				var queryResult = {};
				queryResult.error = err.message;
				returnResult(queryResult);
			} else if (recordset.length === 0 ) {
				returnResult(null);
			} else {
				returnResult(recordset);
			}
        });
    },
	
	
	getTableData: function(databaseName, tableName, returnResult) {
        var totalResult = {};
        this.getPrimaryKeysFromTable(databaseName,tableName,function(recordset){
				totalResult.PK = recordset;
		}); 
		
		this.getForeignKeysFromTable(databaseName,tableName,function(recordset){
               totalResult.FK = recordset;
        });  
		
		this.getRecordsFromTable(tableName,2000,function(recordset) {
                totalResult.Content = recordset;
		});
		
        this.getTableInfo(tableName,function(recordset) {
                totalResult.Collumns = recordset;
		});
		
		this.getTriggersFromTable(tableName,function(recordset) {
                totalResult.Triggers = recordset;
                returnResult(totalResult);
		});
    }
};