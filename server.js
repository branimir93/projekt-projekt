var http = require('http');
var path = require('path');
var socketio = require('socket.io');
var express = require('express');
var funkcije = require('./functions.js');
var auth = require('./auth.js');
var mssql = require('./DB/mssql/mssql_module.js');
var mysql = require('./DB/mysql/mysql-module.js');
var postgresql = require('./DB/postgresql/postgreSQL-module.js');


var router = express();
router.configure(function(){
    router.set('port', process.env.PORT || 3000);
    router.set('view engine', 'ejs');
    router.set('views', __dirname + '/client/views');
    router.use(express.favicon());
    router.use(express.logger('dev'));
    router.use(express.bodyParser());
    router.use(express.methodOverride());
    router.use(express.cookieParser('shhhh, very secret'));
    router.use(express.session());
    
    router.use(function(req, res, next){
        var err = req.session.error,
            msg = req.session.success;
        delete req.session.error;
        delete req.session.success;
        res.locals.message = '';
        if (err) res.locals.message = '<p class="msg error">' + err + '</p>';
        if (msg) res.locals.message = '<p class="msg success">' + msg + '</p>';
        next();
    });

    router.use(router.router);
    router.use(express.static(path.resolve(__dirname, 'client')));
});

var server = http.createServer(router);
var io = socketio.listen(server);


//////////////
// ROUTANJE //
//////////////

/**
 * Prikazuje formu za logiranje te ako je korisnik vec prijavljen redirekta
 * na index stranicu. Takoder ispisuje pogresku (ukoliko dode do nje). 
 */
router.get('/login', function(req, res){

    if(req.session.user) {
        res.redirect('/');
    }
    res.render('beforeLogin/Template', {
        req: req,
        title: 'Login',
        message: res.locals.message,
        body: 'loginBody'
    });
});

/**
 * Prikazuje formu za registraciju
 */
router.get('/register', function(req, res){
    if(req.session.user) res.redirect('/');
    res.render('beforeLogin/Template', {
        req: req,
        title: 'Registracija',
        message: res.locals.message,
        body: 'registerBody'
    });
});

/**
 * Obraduje POST zahtjev za prijavu. provjerava da li je korisnik vec ulogiran,
 * takodjer provjerava jesu li podaci koje je korisnik unio ispravni te ako nisu
 * zapisuje pogresku (te redirekta korisnika na login stranicu). Ukoliko se 
 * korisnik uspije ulogirat zapisuje obavijest te vraca korisnika na prethodnu
 * stranicu.
 */
router.post('/login', function(req, res){
    auth.connectToDB();
    
    auth.authenticate(req.body.username, req.body.password, function(err, user){
        auth.disconnectFromDB();
        if (user) {
            req.session.user = user;

        } else {
            req.session.error = 'Neispravni podaci (isprobaj "demo" and "demo")';
            req.session.loginUsernameValue = req.body.username;
            res.redirect('login');
        }
    });
});

/**
 * Registracija korisnika. Obrađuje se post zahtjev za registraciju korisnika.
 * Ako se unesu neispravni podaci onda se ispisuje poruka sa pogreškom.
 */
router.post('/register', function(req, res){
    auth.connectToDB();
    
    var su = {
        username: req.body.username,
        password: req.body.password,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
    };
        
    auth.addUser(su, function(err) {
        auth.disconnectFromDB();
        if(err) {
            req.session.error = err;
            req.session.registerUsernameValue = req.body.username;
            req.session.registerFnameValue = req.body.fname;
            req.session.registerLnameValue = req.body.lname;
            req.session.registerEmailValue = req.body.email;
            res.redirect('/register');
        } else {
            req.session.success = 'Uspješno ste se registrirali!';
            
            delete req.session.registerUsernameValue;
            delete req.session.registerFnameValue;
            delete req.session.registerLnameValue;
            delete req.session.registerEmailValue;
            
            res.redirect('/login');
        }
    });
    

});

/**
 * Logout korisnika
 */
router.get('/logout', auth.restrict, function(req, res){
    delete req.session.user;
    delete req.session.dbs;
    req.session.success = 'Uspješno ste se odlogirali!';
    res.redirect('/login');
});

/**
 * Prikazuje pocetnu stranicu aplikacije (ukoliko je korisnik ulogiran).
 */
router.get('/', auth.restrict, function(req, res){
    res.render('index/siteTemplate', {
        title: 'Index',
        connectionStatus: '',
        req: req
    });
});

/**
 * Sluzi za prikaz izbornika na lijevoj strani (jsTree). Prima parametre
 * cvora koji korisnik zeli otvoriti te vraca jSON potreban za prikaz.
 */
router.get('/api/izbornik', auth.restrict, function(req, res){
    var vrati, dbid, baza;

    switch (req.query.type){
        case "root":
            auth.connectToDB();
            auth.getDatabasesOfUser(req.session.user,function(result) {
                auth.disconnectFromDB();
                req.session.dbs = result;
                res.json(result); 
            });
            break;

        case "db":
            vrati= "Today it's Sunday";
            res.send(JSON.stringify(vrati));
            break;
            
        case "table":
            try {
                dbid = req.query.id;
                //trazimo bazu pod odgovarajucim id-em
                for(var i = 0; i < req.session.dbs.length; i++) {
                    if(req.session.dbs[i].attr.dbid == dbid) {
                        baza = req.session.dbs[i];
                    }
                }
                //object that keeps database connection
                var databaseConnection;
                //connection object
                var connection = {
                    user: baza.attr.username,
                    password: baza.attr.password,
                    databaseName: baza.attr.databasename,
                    host: baza.attr.server,
                    port: baza.attr.port,
                    dbid: baza.attr.dbid
                };
                
                databaseConnection = funkcije.odrediVrstuBaze(baza.attr.dbtype);
                if(!databaseConnection){
                    throw "NEPOSTOJECAVRSTABAZE";
                }
    
                databaseConnection.connect(connection,function(err) {
                    if(err) {
                        var error = [{
                          data: "Greška prilikom spajanja na bazu!"  
                        }];
                        res.send(JSON.stringify(error));
                        //TODO: send message can't connect
                    }else {
                            databaseConnection.getTables(function(result) {
                                databaseConnection.disconnect();
                                res.send(JSON.stringify(result)); 
                            });
                        }
                });
            } catch(err) {
                switch(err){
                    case "NEPOSTOJECAVRSTABAZE":
                        res.json({}); 
                        break;
                }
            }
                    
            break;
       
         case "procedure":
            dbid = req.query.id;
            //trazimo bazu pod odgovarajucim id-em
            for(var i = 0; i < req.session.dbs.length; i++) {
                if(req.session.dbs[i].attr.dbid == dbid) {
                    baza = req.session.dbs[i];
                }
            }
            //object that keeps database connection
            var databaseConnection;
            //connection object
            var connection = {
                user: baza.attr.username,
                password: baza.attr.password,
                databaseName: baza.attr.databasename,
                host: baza.attr.server,
                port: baza.attr.port,
                dbid: baza.attr.dbid
            };
            
            switch(baza.attr.dbtype){
                case "MYSQL":
                    databaseConnection = mysql;
                    break;
                    
                case "MSSQL":
                    databaseConnection = mssql; 
                    break;
                    
                case "POSTGRES":
                    databaseConnection = postgresql;
                    break;
            }

            databaseConnection.connect(connection,function(err) {
                if(err) {
                    //console.log("POSTGRES: ",err);
                    var error = [{
                      data: "Greška prilikom spajanja na bazu!"  
                    }];
                    res.send(JSON.stringify(error));
                    //TODO: send message can't connect
                }else {
                        databaseConnection.getProcedures(function(result) {
                            databaseConnection.disconnect();
                            res.send(JSON.stringify(result)); 
                        });
                    }
            });
            break;
            
        case "views":
            dbid = req.query.id;
            //trazimo bazu pod odgovarajucim id-em
            for(var i = 0; i < req.session.dbs.length; i++) {
                if(req.session.dbs[i].attr.dbid == dbid) {
                    baza = req.session.dbs[i];
                }
            }
            //object that keeps database connection
            var databaseConnection;
            //connection object
            var connection = {
                user: baza.attr.username,
                password: baza.attr.password,
                databaseName: baza.attr.databasename,
                host: baza.attr.server,
                port: baza.attr.port,
                dbid: baza.attr.dbid
            };
            
            switch(baza.attr.dbtype){
                case "MYSQL":
                    databaseConnection = mysql;
                    break;
                    
                case "MSSQL":
                    databaseConnection = mssql; 
                    break;
                    
                case "POSTGRES":
                    databaseConnection = postgresql;
                    break;
            }

            databaseConnection.connect(connection,function(err) {
                if(err) {
                    //console.log("POSTGRES: ",err);
                    var error = [{
                      data: "Greška prilikom spajanja na bazu!"  
                    }];
                    res.send(JSON.stringify(error));
                    //TODO: send message can't connect
                }else {
                        databaseConnection.getViews(function(result) {
                            databaseConnection.disconnect();
                            res.send(JSON.stringify(result)); 
                        });
                    }
            });
            break;    
            
        default:
            vrati = {};
            res.send(JSON.stringify(vrati));
    }
   

});

/**
 * Povjerava je li moguće ostvariri konekciju na bazu prilikom registracije
 * vraća 
 */
router.post('/registerDBcheckConnection', auth.restrict, function(req, res){
    var connection = {
        user: req.body.username,
        password: req.body.password,
        databaseName: req.body.databasename,
        host: req.body.server,
        port: req.body.port,
    };
    
    var databaseConnection = funkcije.odrediVrstuBaze(req.body.dbtype);
    if(!databaseConnection){
        res.statusCode = 406;   // 406 Not Acceptable
        res.end('Pogrešni podaci!');
        return 0;
    }
    databaseConnection.connect(connection, function(err) {
        databaseConnection.disconnect();
        if(err) {
            res.statusCode = 406;   // 406 Not Acceptable
            res.end(err);
            return 0;
        } else {
            res.statusCode = 200;   // 200 OK
            res.end('Uspiješno spojen!');
            return 0;
        }
    });
    return 0;
});

/**
 * Dodavanje baze podataka za korisnika. 
 */
router.post('/registerDB', auth.restrict, function(req, res){
    var connection = {
        user: req.body.username,
        password: req.body.password,
        databaseName: req.body.databasename,
        host: req.body.server,
        port: req.body.port,
    };
    
    var databaseConnection = funkcije.odrediVrstuBaze(req.body.dbtype);
    if(!databaseConnection){
        res.statusCode = 406;   // 406 Not Acceptable
        res.end('Pogrešni podaci!');
        return 0;
    }
    databaseConnection.connect(connection, function(err) {
        databaseConnection.disconnect();
        if(err) {
            res.statusCode = 406;   // 406 Not Acceptable
            res.end(err.message);
            return 0;
        } else {
            auth.connectToDB();
            var su = {
                dbalias: req.body.dbalias,
                dbtype: req.body.dbtype,
                username: req.body.username,
                password: req.body.password,
                databasename: req.body.databasename,
                server: req.body.server,
                port: req.body.port,
                userid: req.session.user.userid,
            };
            auth.addDB(su, req.session.user.username, function(err) {
                auth.disconnectFromDB();
                if(err) {
                    res.statusCode = 406;
                    res.end(err.message);
                    return 0;
                } else {
                    res.statusCode = 200;
                    res.end('Uspješno ste dodali bazu! Osvježite stranicu za pregled.');
                    return 0;
                }       
            });
            return 0;
        }
    });
    return 0;
});

/**
 * Prikazuje formu za registraciju baze
 */
router.get('/registerDB', auth.restrict, function(req, res){
    res.render('index/registerDBBody', {
        req: req,
        connectionStatus : '',
        body: 'registerDBBody'
    });
});



/**
 * Prikazuje procedure
 */
router.post('/api/procedure', auth.restrict, function(req, res){
  /* res.render('index/procedure', {
        title:'procedure',
   }); */
   
    var dbid = req.body.dbid;
    var procname=req.body.procname;
    var baza = auth.returnDB(dbid, req.session.dbs);
    
    if(baza){
        
        res.render('index/procedure', {
            dbid: dbid,
            procname:procname,
        }); 
    }
    else {
        res.statusCode = 403;   // 403 Forbidden
        res.end('Niste vlasnik baze!');
    }
});



router.post('/api/showProcedure', auth.restrict, function(req, res){
  var dbid = req.body.dbid;
   var procname=req.body.procname;
    var baza = auth.returnDB(dbid, req.session.dbs);
    
    if(baza){
        var connectionObject;
        switch(baza.attr.dbtype){
            case "MYSQL":
                connectionObject = mysql;
                break;
            case "MSSQL":
                connectionObject = mssql;
                break;
            case "POSTGRES":
                connectionObject = postgresql;
                break;
        }
        
        var connection = {
            user: baza.attr.username,
            password: baza.attr.password,
            databaseName: baza.attr.databasename,
            host: baza.attr.server,
            port: baza.attr.port
        };
    
        connectionObject.connect(connection,function(err) {
            if(err) {
               console.log(err);
            }else {
                connectionObject.getProcedureData(procname,function(result) {
                    
                    if(result.error) {
                        console.log(result.error);
                    }else {
                        connectionObject.disconnect();
                        res.json(result.result);
                    }
               
                });
            }
        });
    }
});


router.post('/api/view', auth.restrict, function(req, res){

   
    var dbid = req.body.dbid;
    var viewname=req.body.viewname;
    var baza = auth.returnDB(dbid, req.session.dbs);
    
    if(baza){
        
        res.render('index/views', {
            dbid: dbid,
            viewname:viewname,
        }); 
    }
    else {
        res.statusCode = 403;   // 403 Forbidden
        res.end('Niste vlasnik baze!');
    }
});



router.post('/api/showView', auth.restrict, function(req, res){
  var dbid = req.body.dbid;
   var viewname=req.body.viewname;
    var baza = auth.returnDB(dbid, req.session.dbs);
    
    if(baza){
        var connectionObject;
        switch(baza.attr.dbtype){
            case "MYSQL":
                connectionObject = mysql;
                break;
            case "MSSQL":
                connectionObject = mssql;
                break;
            case "POSTGRES":
                connectionObject = postgresql;
                break;
        }
        
        var connection = {
            user: baza.attr.username,
            password: baza.attr.password,
            databaseName: baza.attr.databasename,
            host: baza.attr.server,
            port: baza.attr.port
        };
    
        connectionObject.connect(connection,function(err) {
            if(err) {
               console.log(err);
            }else {
                connectionObject.getViewData(viewname,function(result) {
                    if(!result) {
                        console.log("Ne postoji view pod tim imenom");
                    }
                    if(result.error) {
                        console.log(result.error);
                    }else {
                        connectionObject.disconnect();
                        res.json(result.result);
                    }
               
                });
            }
        });
    }
});



/**
 * Prikazuje Novi tab za bazu podataka
 */
router.get('/api/openDB/:id', auth.restrict, function(req, res){
    var baza = auth.returnDB(req.params.id, req.session.dbs);
    
    if(baza){
        res.render('index/baza.ejs', {
            dbid: req.params.id,
        }); 
    }
    else {
        res.statusCode = 403;   // 403 Forbidden
        res.end('Niste vlasnik baze!');
    }
});

/**
 * Prikazuje editor za SQL upite
 */
router.get('/api/SQL/:id', auth.restrict, function(req, res){
    var baza = auth.returnDB(req.params.id, req.session.dbs);
    
    if(baza){
        res.render('index/sql_editor', {
            title:'SQL',
            message: res.locals.message,
            dbid: req.params.id,
        }); 
    }
    else {
        res.statusCode = 403;   // 403 Forbidden
        res.end('Niste vlasnik baze!');
    }
});

/**
 * Vraća rezultat SQL upita
 */
router.post('/api/SQL', auth.restrict, function(req, res){
    var dbid = req.body.dbid;
    var baza = auth.returnDB(dbid, req.session.dbs);
    
    if(baza){
        var connectionObject;
        switch(baza.attr.dbtype){
            case "MYSQL":
                connectionObject = mysql;
                break;
            case "MSSQL":
                connectionObject = mssql;
                break;
            case "POSTGRES":
                connectionObject = postgresql;
                break;
        }
        
        var connection = {
            user: baza.attr.username,
            password: baza.attr.password,
            databaseName: baza.attr.databasename,
            host: baza.attr.server,
            port: baza.attr.port
        };
        
        connectionObject.connect(connection,function(err) {
            if(err) {
               console.log(err);
            }else {
                connectionObject.executeQuery(req.body.code,function(result){
                    connectionObject.disconnect();
                    res.json(result); //salje se rezultat querija
                });
            }
        });
    }
    else {
        res.statusCode = 403;   // 403 Forbidden
        res.end('Niste vlasnik baze!');
    }

});

/**
 * Prikazuje editor za SQL upite
 */
router.post('/showTableTabs', auth.restrict, function(req, res){
    var dbid = req.body.dbid;
    var tableName = req.body.name;
    var baza = auth.returnDB(dbid, req.session.dbs);
    
    if(baza){
        
        res.render('index/taboviTablice', {
            dbid: dbid,
            tableName: tableName,
        }); 
    }
    else {
        res.statusCode = 403;   // 403 Forbidden
        res.end('Niste vlasnik baze!');
    }
});

/**
 * Prikazuje query object
 */
router.post('/showTable', function(req, res){
    var dbid = req.body.dbid;
    var baza = auth.returnDB(dbid, req.session.dbs);
    
    if(baza){
        var connectionObject;
        switch(baza.attr.dbtype){
            case "MYSQL":
                connectionObject = mysql;
                break;
            case "MSSQL":
                connectionObject = mssql;
                break;
            case "POSTGRES":
                connectionObject = postgresql;
                break;
        }
        
        var connection = {
            user: baza.attr.username,
            password: baza.attr.password,
            databaseName: baza.attr.databasename,
            host: baza.attr.server,
            port: baza.attr.port
        };
        
        connectionObject.connect(connection,function(err) {
            if(err) {
               console.log(err);
            }else {
                var totalResult = {};
                
                connectionObject.getPrimaryKeysFromTable(connection.databaseName, req.body.name, function(result){
                    totalResult.PK = result;
                   
                    connectionObject.getForeignKeysFromTable(connection.databaseName, req.body.name,function(result){
                        totalResult.FK = result;
                        
                        connectionObject.getRecordsFromTable(req.body.name,"20000",function(result){
                            totalResult.Content = result;
                                
                            connectionObject.getTableInfo(req.body.name,function(result){
                                    
                                    connectionObject.disconnect();
                                    totalResult.Collumns = result;
                                    res.json(totalResult);
                                
                            });
                        });
                    });
                });
            }
        });
    }
    else {
        res.statusCode = 403;   // 403 Forbidden
        res.end('Niste vlasnik baze!');
    }
});


router.post('/getName', function(req, res){
    var dbid = req.body.dbid;
    var baza = auth.returnDB(dbid, req.session.dbs);
    if(baza){
        res.end(baza.data); 
    }
});

///////////////////
// KRAJ ROUTANJA //
///////////////////


/**
 * Pali server! :)
 */
server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Server listening at", addr.address + ":" + addr.port);
});