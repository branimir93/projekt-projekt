var mssql = require('./DB/mssql/mssql_module.js');
var mysql = require('./DB/mysql/mysql-module.js');
var postgresql = require('./DB/postgresql/postgreSQL-module.js');

module.exports = {
    
    /**
     * Vraća modul potreban za konekciju ili dojavljuje grešku
     * (provjeri sa if(result) )
     */
    odrediVrstuBaze: function(type){
        var databaseConnection;
        switch(type){
            case "MYSQL":
                databaseConnection = mysql;
                break;
                
            case "MSSQL":
                databaseConnection = mssql; 
                break;
                
            case "POSTGRES":
                databaseConnection = postgresql;
                break;
            default:
                break;
        }
        
        return databaseConnection;
    },
    
    
};