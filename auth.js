var loginDatabase = require("./DB/postgresql/postgreSQL-login-module");

module.exports = {
    
    disconnectFromDB: function() {
        loginDatabase.disconnect();
    },
    
    connectToDB: function(){
        var connection = {
            user: "postgres",
            password: "root",
            host: "localhost",
            port: "5432",
            databaseName: "Login",
        };
	
        loginDatabase.connect(connection,function(err) {
            if(err !== null) {
                console.log("Error conecting to Login database.");
                console.log(err);
            }
        });
    },
    
    /**
     * Provjerava ispravnost korisnickih podataka te vraca korisnikov objekt
     * ukoliko su uneseni podaci dobri.
     * 
     * @param {String} korisnikov username
     * @param {String} korisnikov password
     * @param {Function} callback
     */
    authenticate: function(name, pass,  fn) {

        loginDatabase.getUserByUsername(name,function(user) {
            if(user === false) {
                fn(new Error('Cannot find user'));
            }
            if(user.password == pass) {
                fn(null,user);
            }
            fn(new Error('Invalid password'));
        });
    },
    
    /**
     * Provjerava da li je korisnik ulogiran te ako nije postavlja poruku o
     * gresci te redirekta korisnika na login.
     */
    restrict: function(req, res, next) {
        if (req.session.user) {
            next();
        } else {
            req.session.error = 'Morate se ulogirati!';
            res.redirect('/login');
        }
    },
    
    validateEmail: function (email) { 
   
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       
        return re.test(email);
    }, 
    
    /**
     * Funkcija dodaje korisnika u bazu podataka. User objekt mora imati sljedeće varijable:
     * username, password, fname, lname, email. Ako već postoji korisničko ime onda se pogreška obrađuje u errorFunction.
     * Ona prima err parametar tj. opis pogreške.
     * 
     */
    addUser: function(us, errorFunction) {
        if(us.username === "" || us.password === "" || 
            us.fname === "" || us.lname === "" || us.email === "") {
                errorFunction("Unesite ispravne podatke.");
        } else if(this.validateEmail(us.email) === false) {
            errorFunction("Unesite ispravnu e-mail adresu.");
        }else {
            loginDatabase.getUserByUsername(us.username, function(user) {
                if(user === false) {
                    loginDatabase.addUser(us,errorFunction);
                }else {
                    errorFunction("Već postoji korisnik pod tim korisničkim imenom");
                }
            });
        }
    },
    
    /**
     * Fukcija vraća sve baze podataka koje je korisnik spremio. 
     * Vraća ih u obliku JSON objekta koji se može koristiti u JSONTree.
     *     
     */
    getDatabasesOfUser: function(user, processResult) {

        loginDatabase.getDatabasesOfUserByUsername(user.username,function(result) {
            processResult(result);
        });
    },
    
    /**
     * Funkcija dodaje bazu podataka korisnika u bazu Login. 
     * 
     */
    addDB: function(database,username,processResult) {
        loginDatabase.addUserDatabase(username,database,processResult);
    },
    
    returnDB: function(dbid, listOfDB){
        for(var i = 0; i < listOfDB.length; i++) {
            if(listOfDB[i].attr.dbid == dbid) {
                return listOfDB[i];
            }
        }
    }
};