function prikazi_obavijest(poruka, class_of_message){
    //pripremi poruku za prikaz
    poruka = '<div class="' + class_of_message + '">' + poruka + '</div>';
    
    //ako je poruka vec prikazana
    if($("#obavijest").is(':visible')){
        $("#obavijest_tekst").append(poruka);
        return;
    }
    
    $("#obavijest_tekst").html(poruka);
    
    $("#obavijest").fadeIn(100);
    positionPopup();
    
    $("#close").click(function(){
        $("#obavijest").fadeOut(100);
        $("#obavijest_tekst").html('');
    });
}
 
function positionPopup(){
    if(!$("#obavijest").is(':visible')){
        return;
    }
    $("#obavijest").css({
        left: ($(window).width() - $('#obavijest').width()) / 2,
        top: ($(window).width() - $('#obavijest').width()) / 7,
        position:'absolute'
    });
}

$(window).bind('resize',positionPopup);