$("#registerDB").on('click',function(event){
    event.preventDefault();
    
    $("#registracijaBazeForm").ajaxSubmit({
        url: '/registerDB', 
        type: 'post',
        error: function(returnval) {
            prikazi_obavijest(returnval, "error");
        },
        success: function (returnval) {
            prikazi_obavijest(returnval, "success"); 
        }
    });
    
});

$("#registerDBcheck").on('click',function(event){
    event.preventDefault();
    $("#registracijaBazeForm").ajaxSubmit({
        url: '/registerDBcheckConnection', 
        type: 'post',
        error: function(returnval) {
            prikazi_obavijest(returnval, "error");
        },
        success: function (returnval) {
            prikazi_obavijest(returnval, "success"); 
        }
    });
    
});