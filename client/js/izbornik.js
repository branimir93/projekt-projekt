 var otvoreni = [];
    
 $(function () {
    $("#stablo")
        .jstree({ 
            "json_data" : {
                "ajax" : { 
                    "url" : "api/izbornik",
                    "data" : function (n) {
                        if(n.attr){
                            return { 
                                id : n.attr("dbid"),
                                type: n.attr("type"),
                            };
                        }
                        else {
                            return { type: "root" };
                        }
                    }
                }
            },
            "themes" : {
                "theme" : "default",
                "dots" : true,
                "icons" : true
            },
            "plugins" : [ "themes", "json_data","ui" ],
        })
        .bind("select_node.jstree", function (event, data) {
            
            //Otvori TAB
            var dbid = data.rslt.obj.attr("dbid");
            var request = $.ajax({
                url: "/api/openDB/"+dbid,
                type: "get",
            });
            
            request.success(function (response, textStatus, jqXHR){
                
                var imeBaze='';
                var requestName = $.ajax({
                    url: "/getName",
                    type: "post",
                    data: {
                        dbid: data.rslt.obj.attr("dbid"),
                    }
                });
                requestName.success(function (responseDB, textStatus, jqXHR){
                    imeBaze=responseDB;
            
                    //provjeri je li tab vec otvoren
                    if(otvoreni.indexOf(data.rslt.obj.attr("dbid"))<0){
                        otvoreni.push(data.rslt.obj.attr("dbid"));
                       
                        $('#dbtabs').append(
                            $('<li><a href="#dbid' + dbid + '" data-toggle="tab">' + imeBaze +
                                    '<button class="close" title="Remove this page" type="button">×</button>' +
                            '</a></li>'));
                        $("#content").append(response);
                        $('#dbtabs a[href^="#dbid'+dbid +'"]').tab('show');
                        stvori_sql_editor(dbid);
                        izvrsiObicanSql(dbid);
                        izvrsiSelektiranSql(dbid);
                        $("#tabovi").on("click", "a", function(e) {
                            e.preventDefault();
                            $(this).tab('show');
                        });
                        
                        //Zatvaranje tabova
                        $('#tabsdbid' + dbid).on('click', ' li a .close', function() {
                               
                            var tabId = $(this).parents('li').children('a').attr('href');
                            $(this).parents('li').remove('li');
                            $(tabId).remove();
                            $('#tabsdbid' + dbid + ' a:first').tab('show');
                        });
                      
                    }
                    else {
                        $('#dbtabs a[href^="#dbid'+dbid +'"]').tab('show');
                    }
                });
            
                //Zavisi o opciji
                switch(data.rslt.obj.attr("type")){
                    case "db":
                        
                        break;
                        
                    case "table":
                        // prepoznaje razliku jel oznacena neka tablica ili "Table" u izborniku
                        if (!data.rslt.obj.attr("name")){
                            console.log(data.rslt.obj.attr("name"));
                        }
                        else {
                            var requestTabova = $.ajax({
                                url: "/showTableTabs",
                                type: "post",
                                data: {
                                    dbid: data.rslt.obj.attr("dbid"),
                                    name: data.rslt.obj.attr("name"),
                                }
                            });
                            
                            
                            var tablename = data.rslt.obj.attr("name");
                            requestTabova.success(function(responseTabova, textStatus, jqXHR){
                                request = $.ajax({
                                    url: "/showTable",
                                    type: "post",
                                    data: {
                                        dbid: dbid,
                                        name: tablename,
                                    }
                                });
                                request.success(function(response, textStatus, jqXHR){
                                    //otvori tab za prikaz tablice
                                    $('#tabsdbid' + dbid).append(
                                        $('<li><a href="#table' + dbid+'-' + tablename + '" data-toggle="tab">Tablica: ' + tablename+
                                                '<button class="close" title="Remove this page" type="button">×</button>' +
                                        '</a></li>'));
                                    
                                    $("#dbidcontent"+dbid).append(responseTabova);
                                    
                                    //Napuni tablice podacima
                                    for (var key in response) {
                                        if(key=='Content'){
                                            $("#"+key + dbid + tablename).html(
                                                ConvertJsonToTable(response[key].result, key+'tablica_tab' + dbid + tablename, null, 'Download')
                                            );
                                        }
                                        else{
                                            $("#"+key + dbid + tablename).html(
                                                ConvertJsonToTable(response[key], key + 'tablica_tab' + dbid + tablename, null, 'Download')
                                            );
                                        }
                                        $('#' + key + 'tablica_tab' + dbid + tablename).dataTable();
                                        
                                    }
                                    
                                    //Otvori taj tab
                                    $('#taboviTablice' + dbid + ' a[href^="#Collumns' + dbid + tablename + '"]').tab('show');
                            
                                    //Otvori vanjski tab
                                    $('#tabsdbid' + dbid + ' a[href^="#table' + dbid+'-' + tablename + '"]').tab('show');
                                });
                            });
                            
                        }
                        break;
                        
                    case "procedure":
                        // prepoznaje razliku jel oznacena neka tablica ili "Table" u izborniku
                        if (!data.rslt.obj.attr("name")){
                            console.log(data.rslt.obj.attr("name"));
                        }
                        else {
                            var requestProcedura = $.ajax({
                                url: "/api/procedure",
                                type: "post",
                                data: {
                                    dbid: data.rslt.obj.attr("dbid"),
                                    procname: data.rslt.obj.attr("name"),
                                }
                            });
                            
                            requestProcedura.success(function(responseTabova, textStatus, jqXHR){
                                var dbid = data.rslt.obj.attr("dbid");
                                var procname = data.rslt.obj.attr("name");
                                request = $.ajax({
                                    url: "/api/showProcedure",
                                    type: "post",
                                    data: {
                                        dbid: dbid,
                                        procname: procname,
                                    }
                                });
                                request.success(function(response, textStatus, jqXHR){
                                    //otvori tab za prikaz tablice
                                    $('#tabsdbid' + dbid).append(
                                        $('<li><a href="#procedure' + dbid + '-' + procname + '" data-toggle="tab">Procedura: ' + procname +
                                                '<button class="close" title="Remove this page" type="button">×</button>' +
                                        '</a></li>'));
                                    
                                    $("#dbidcontent"+dbid).append(responseTabova);
                                    $("#nameProcedure" + dbid + '-' + procname).html(response.procname);
                                    
                                    //Otvori vanjski tab
                                    $('#tabsdbid' + dbid + ' a[href^="#procedure' + dbid+'-' + procname + '"]').tab('show');
                                    
                                    $("#srcProcedure" + dbid + '-' + procname).html('');
                                    CodeMirror(document.getElementById("srcProcedure" + dbid + '-' + procname), {
                                        value: response.procsrc,
                                        mode:'text/x-sql',
                                        lineNumbers: true,
                                        readOnly: true
                                    });
                                    
                                });
                            });
                            
                        }
                        break;
                        
                    case "view":
                        // prepoznaje razliku jel oznacena neka tablica ili "Table" u izborniku
                        if (!data.rslt.obj.attr("name")){
                            console.log(data.rslt.obj.attr("name"));
                        }
                        else {
                            var requestView = $.ajax({
                                url: "/api/view",
                                type: "post",
                                data: {
                                    dbid: data.rslt.obj.attr("dbid"),
                                    viewname: data.rslt.obj.attr("name"),
                                }
                            });
                            
                            requestView.success(function(responseTabova, textStatus, jqXHR){
                                var dbid = data.rslt.obj.attr("dbid");
                                var viewname = data.rslt.obj.attr("name");
                                request = $.ajax({
                                    url: "/api/showView",
                                    type: "post",
                                    data: {
                                        dbid: dbid,
                                        viewname: viewname,
                                    }
                                });
                                request.success(function(response, textStatus, jqXHR){
                                    //otvori tab za prikaz tablice
                                    $('#tabsdbid' + dbid).append(
                                        $('<li><a href="#view' + dbid + '-' + viewname + '" data-toggle="tab">Pogled: ' + viewname +
                                                '<button class="close" title="Remove this page" type="button">×</button>' +
                                        '</a></li>'));
                                    
                                    $("#dbidcontent"+dbid).append(responseTabova);
                                    $("#nameView" + dbid + '-' + viewname).html(response.viewname);
                                    
                                    //Otvori vanjski tab
                                    $('#tabsdbid' + dbid + ' a[href^="#view' + dbid+'-' + viewname + '"]').tab('show');
                                    
                                    $("#srcView" + dbid + '-' + viewname).html('');
                                    CodeMirror(document.getElementById("srcView" + dbid + '-' + viewname), {
                                        value: response.viewsrc,
                                        mode:'text/x-sql',
                                        lineNumbers: true,
                                        readOnly: true
                                    });
                                    
                                });
                            });
                            
                        }
                        break;
                }
            });
            request.error(function(obj,text,error) {
                prikazi_obavijest(obj.responseText, "error");
                return 0;
            });
            
            
            // Zatvaranje tabova
            $('#dbtabs').on('click', ' li a .close', function() {
                    var tabId = $(this).parents('li').children('a').attr('href');
                    $(this).parents('li').remove('li');
                    $(tabId).remove();
                    $('#dbtabs a:first').tab('show');
                    $.each(otvoreni, function(i){
                        if(otvoreni[i] == data.rslt.obj.attr("dbid")) {
                            otvoreni.splice(i,1);
                            return false;
                        }
                    });
            
            
            });
    
      
        });
        /**
         * Click Tab to show its contents
         */
        $("#dbtabs").on("click", "a", function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        
        $("#tabovi").on("click", "a", function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
});