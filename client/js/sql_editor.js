//za sql editor
var editori = {};
var pageNum=0; 
function stvori_sql_editor(dbid){
    console.log(editori);
    editori[dbid] = CodeMirror.fromTextArea(document.getElementById('code' + dbid), {
        mode:'text/x-sql',
        indentWithTabs: true,
        smartIndent: true,
        lineNumbers: true,
        matchBrackets : true,
    });
}

function izvrsiObicanSql(dbid){
    $("#izvrsiSQL" + dbid).on('submit',function(event){
         var dbid = $(this).find( "input[name='dbid']" ).val();
         
       
        event.preventDefault();
        var request;
       
        var array = editori[dbid].getValue().split(";");
        for (var i=0;i<array.length-1;i++){
        
        request = $.ajax({
            url: "/api/SQL",
            type: "post",
            data: {
                dbid: dbid,
                code: array[i]+";",
            }
        });
        
        request.success(function (response, textStatus, jqXHR){
            pageNum++;
            if(!response.error){
                //Stvara novi tab u izborniku
                $('#izvrsiSQLtabovi' + dbid).append(
                    $('<li><a href="#rezultatSQL' + dbid + '-' + pageNum + '" data-toggle="tab">' 
                        + pageNum+ '<button class="close" title="Remove this page" type="button">×</button>' +
                    '</a></li>'));
                
                //stvara novo polje koje prikazuje taj tab
                $('#rezultatSQL' + dbid).append(
                    $('</pre><div class="tab-pane" id="rezultatSQL' + dbid + '-' + pageNum + '">'+
                        '<table id="tablica' + dbid + '-' + pageNum + '" style="display:none">' +
                        '</table>'  +
                    '</div><pre>'));
                    
                //Napuni tablicu podacima
                $('#rezultatSQL' + dbid + '-' + pageNum).html(
                    ConvertJsonToTable(response.result, 'rezultatSQLtablica' + dbid + '-' + pageNum, null, 'Download')
                ); 
                
                //u tom polju prikazuje tu tablicu
                $('#rezultatSQLtablica' + dbid + '-' + pageNum).dataTable({"bProcessing": true});
                
                //Otvori taj tab
                $('#izvrsiSQLtabovi' + dbid + ' a[href^="#rezultatSQL' + dbid + '-' + pageNum + '"]').tab('show');
        
                //Zatvaranje tabova
                $('#izvrsiSQLtabovi' + dbid).on('click', ' li a .close', function() {
                       
                        var tabId = $(this).parents('li').children('a').attr('href');
                        $(this).parents('li').remove('li');
                        $(tabId).remove();
                        $('#izvrsiSQLtabovi' + dbid + ' a:first').tab('show');
                });
            }
            else {
                
               
                //Stvara novi tab u izborniku
                $('#izvrsiSQLtabovi' + dbid).append(
                    $('<li><a href="#rezultatSQL' + dbid + '-' + pageNum + '" data-toggle="tab">' 
                        + pageNum+ '<button class="close" title="Remove this page" type="button">×</button>' +
                    '</a></li>'));
                
                //stvara novo polje koje prikazuje taj tab
                $('#rezultatSQL' + dbid).append(
                    $('</pre><div class="tab-pane" id="rezultatSQL' + dbid + '-' + pageNum + '">'+
                        response.error  +
                    '</div><pre>'));
                    
                    
                $('#rezultatSQL' + dbid + '-' + pageNum).html(response.error);
                    
                //Otvori taj tab
                $('#izvrsiSQLtabovi' + dbid + ' a[href^="#rezultatSQL' + dbid + '-' + pageNum + '"]').tab('show');
                
                $('#izvrsiSQLtabovi' + dbid).on('click', ' li a .close', function() {
                       
                        var tabId = $(this).parents('li').children('a').attr('href');
                        $(this).parents('li').remove('li');
                        $(tabId).remove();
                     
                        $('#izvrsiSQLtabovi' + dbid + ' a:first').tab('show');
                });
            }
        });
        }
    });

}



function izvrsiSelektiranSql(dbid){
    $("#izvrsiselektiranSQL" + dbid).on('submit',function(event){
         var dbid = $(this).find( "input[name='dbid']" ).val();
         
        
        event.preventDefault();
        var request;
        var last= editori[dbid].getSelection().charAt(editori[dbid].getSelection().length-1);
        var iterator
        var array = editori[dbid].getSelection().split(";");
        if(last==';'){
            iterator=array.length-1;
        }
        else{
            iterator=array.length;
        }
        for (var i=0;i<iterator;i++){
        request = $.ajax({
            url: "/api/SQL",
            type: "post",
            data: {
                dbid: dbid,
                code: array[i]+";",
            }
        });
        
        request.success(function (response, textStatus, jqXHR){
            pageNum++;
            if(!response.error){
                //Stvara novi tab u izborniku
                $('#izvrsiSQLtabovi' + dbid).append(
                    $('<li><a href="#rezultatSQL' + dbid + '-' + pageNum + '" data-toggle="tab">' 
                        + pageNum+ '<button class="close" title="Remove this page" type="button">×</button>' +
                    '</a></li>'));
                
                //stvara novo polje koje prikazuje taj tab
                $('#rezultatSQL' + dbid).append(
                    $('</pre><div class="tab-pane" id="rezultatSQL' + dbid + '-' + pageNum + '">'+
                        '<table id="tablica' + dbid + '-' + pageNum + '" style="display:none">' +
                        '</table>'  +
                    '</div><pre>'));
                    
                //Napuni tablicu podacima
                $('#rezultatSQL' + dbid + '-' + pageNum).html(
                    ConvertJsonToTable(response.result, 'rezultatSQLtablica' + dbid + '-' + pageNum, null, 'Download')
                );
                
                //u tom polju prikazuje tu tablicu
                $('#rezultatSQLtablica' + dbid + '-' + pageNum).dataTable({"bProcessing": true});
                
                //Otvori taj tab
                $('#izvrsiSQLtabovi' + dbid + ' a[href^="#rezultatSQL' + dbid + '-' + pageNum + '"]').tab('show');
        
                //Zatvaranje tabova
                $('#izvrsiSQLtabovi' + dbid).on('click', ' li a .close', function() {
                       
                        var tabId = $(this).parents('li').children('a').attr('href');
                        $(this).parents('li').remove('li');
                        $(tabId).remove();
                        $('#izvrsiSQLtabovi' + dbid + ' a:first').tab('show');
                });
            }
            else {
                
               
                //Stvara novi tab u izborniku
                $('#izvrsiSQLtabovi' + dbid).append(
                    $('<li><a href="#rezultatSQL' + dbid + '-' + pageNum + '" data-toggle="tab">' 
                        + pageNum+ '<button class="close" title="Remove this page" type="button">×</button>' +
                    '</a></li>'));
                
                //stvara novo polje koje prikazuje taj tab
                $('#rezultatSQL' + dbid).append(
                    $('</pre><div class="tab-pane" id="rezultatSQL' + dbid + '-' + pageNum + '">'+
                        response.error  +
                    '</div><pre>'));
                    
                
                //Napuni tablicu podacima
                $('#rezultatSQL' + dbid + '-' + pageNum).html(response.error);
                    
                //Otvori taj tab
                $('#izvrsiSQLtabovi' + dbid + ' a[href^="#rezultatSQL' + dbid + '-' + pageNum + '"]').tab('show');
                
                $('#izvrsiSQLtabovi' + dbid).on('click', ' li a .close', function() {
                       
                        var tabId = $(this).parents('li').children('a').attr('href');
                        $(this).parents('li').remove('li');
                        $(tabId).remove();
                     
                        $('#izvrsiSQLtabovi' + dbid + ' a:first').tab('show');
                });
            }
        });
        }
    });

}

